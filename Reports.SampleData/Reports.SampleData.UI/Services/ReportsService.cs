﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Xml.Serialization;
using Reports.SampleData.UI.ReportsClasses;
using TestingPayloadForCharts.Models;
using System.Collections;
using Reports.SampleData.UI.ViewModels;

namespace Reports.SampleData.UI.Services
{
    public class DouchNutData {
        public int DocumentId { get; set; }
        public IEnumerable<References> Previous { get; set; }
        public IEnumerable<References> Next { get; set; }
    }

    public class References {
        public string Id { get; set; }
        public string NameOrSearchTerm { get; set; }
        public int Count { get; set; }

        public string Type { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Reports.SampleData.UI.Services.IReportsService" />
    public class ReportsService : IReportsService
    {
        /// <summary>
        /// The _json collection
        /// </summary>
        public readonly IEnumerable<PayloadObject> _accessDataCollection;

        /// <summary>
        /// The _user data collection
        /// </summary>
        public List<UserData> _userDataCollection;

        /// <summary>
        /// The _resource data collection
        /// </summary>
        public readonly IEnumerable<ResourceData> _resourceDataCollection;

       

        private ResourceData GetDataById(int id)
        {
            return _resourceDataCollection.FirstOrDefault(i => i.Id == id);
        }

        private string EquateType(string eqType)
        {
            
            if(GetId(eqType) != null)
            {
                return "Document";
            }

            if (eqType.StartsWith("ST~"))
            {
                return "Search";
            }



            return eqType; 
        }


        private string GetSearchTermOrName(string eqType)
        {
            // If Doc
            var res = GetId(eqType);
            if (res != null)
                return GetDataById((int)res).Name;

            // If Search
            if (eqType.StartsWith("ST~"))
            {
                return System.Net.WebUtility.UrlDecode(eqType.Substring(3));
            }

            return eqType;
        }

        private int? GetId(string eqType)
        {
            int docid;
            if (int.TryParse(eqType, out docid))
            {
                return docid;
            }
            return null;
        }
        private DouchNutData Fn(int documentId)
        {
            var dd = new DouchNutData()
            {
                DocumentId = documentId,
                Previous = new List<References>()
            };

            // build From Ref
            var fromRef = new List<References>();
            var gb = _accessDataCollection.Where(r => r.DocId == documentId).GroupBy(gc => gc.Referer.EquatedType);
            var gcc = gb.Select(s => new References
            {
                Id = GetId(s.Key) != null ? GetId(s.Key).ToString() : "",
                NameOrSearchTerm = GetSearchTermOrName(s.Key),
                Type = EquateType(s.Key),
                Count = s.Count()
            }).OrderByDescending(ob => ob.Count).ToList();




            // Build a list per document
            dd.Previous = gcc;


            return dd;
        }

        private DouchNutData Fn2(int documentId)
        {
            var dd = new DouchNutData()
            {
                DocumentId = documentId,
                Previous = new List<References>()
            };

            // build From Ref
            var fromRef = new List<References>();
            var gb = _accessDataCollection.Where(r => r.Referer != null && 
                                                        !string.IsNullOrWhiteSpace(r.Referer.DocId) && 
                                                        r.Referer.DocId.Trim().Equals(documentId.ToString().Trim(), StringComparison.InvariantCultureIgnoreCase))
                                                        .GroupBy(gc => gc.Referer.EquatedType);
            var gcc = gb.Select(s => new References
            {
                Id = GetId(s.Key) != null ? GetId(s.Key).ToString() : "",
                NameOrSearchTerm = GetSearchTermOrName(s.Key),
                Type = EquateType(s.Key),
                Count = s.Count()
            }).OrderByDescending(ob => ob.Count).ToList();




            // Build a list per document
            dd.Previous = gcc;


            return dd;
        }

        public DouchNutData GetPreviousReferences(int documentId)
        {
            return Fn(documentId);
        }

        public DouchNutData GetNextReferences(int documentId)
        {
            return Fn2(documentId);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportsService"/> class.
        /// </summary>
        public ReportsService()
        {
            var accessJsonData = File.ReadAllText(
                HttpContext.Current.Server.MapPath("~/App_Data/processed_filtered.json"));
            _accessDataCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<PayloadObject>>(accessJsonData);
            _accessDataCollection = _accessDataCollection.Where(rs => rs.Hostname.ToLower().Equals("production.bluecare.supportpoint.com"));

            var resourceDataJson = File.ReadAllText(HttpContext.Current.Server.MapPath("~/App_Data/resource_data.json"));
            _resourceDataCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<ResourceData>>(resourceDataJson);
            _accessDataCollection.ToList().ForEach(i =>
            {
                // lookup
                var entry = _resourceDataCollection.FirstOrDefault(e => e.Id == i.DocId);

                i.ResourceProperties = entry;
            });

            var userData = File.ReadAllLines(HttpContext.Current.Server.MapPath("~/App_Data/users.csv"));
            _userDataCollection = new List<UserData>();
            userData.ToList().ForEach(i =>
            {
                var items = i.Split(',');
                var ud = new UserData()
                {
                    Id = int.Parse(items[0]),
                    FirstName = items[1],
                    LastName = items[2],
                    UserName = items[3]
                };
                _userDataCollection.Add(ud);
            });
        }


        public ChartConfig GetTop10Documents_Template()
        {
            return new ConcreteCreator().FactoryMethod();
        }

        public ChartConfig GetDocsVFolders_Template()
        {
            return new ChartConfig
            {


                Title = new Title
                {
                    Text = "Folder/Document access trends"
                },
                Subtitle = new Subtitle() { Text = "Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in" },
                Series = new List<Series>(new Series[]
                {
                    new Series()
                    {
                        Name = "Documents",
                        Data = new List<Data>()
                    },

                    new Series()
                    {
                        Name = "Folders",
                        Data = new List<Data>()
                    }
                }),
                Options = new Options
                {
                    Chart = new Chart
                    {
                        Type = "area",
                        ZoomType = "x"
                    }
                },
                XAxis = new XAxis()
                {
                    StartOnTick = true,
                    Type = "datetime",
                    DateTimeLabelFormats = new DateTimeLabelFormats() { Month = "%b %e, %Y" },
                },
                YAxis = new YAxis()
                {
                    Title = new Title()
                    {
                        Text = "no. of accesses"
                    }
                },
                Legend = new Legend() { Enabled = false }
            };
        }

        public ChartConfig NextPrevious_Template()
        {
            return new ChartConfig
            {


                Title = new Title
                {
                    Text = "Folder/Document access trends"
                },
                Series = new List<Series>(new Series[]
                {
                    new Series()
                    {
                        Name = "Documents",
                        Data = new List<Data>()
                    },

                    new Series()
                    {
                        Name = "Folders",
                        Data = new List<Data>()
                    }
                }),
                Options = new Options
                {
                    Chart = new Chart
                    {
                        Type = "pie"
                    }
                },
                XAxis = new XAxis()
                {
                    StartOnTick = true,
                    Type = "datetime",
                    DateTimeLabelFormats = new DateTimeLabelFormats() { Month = "%b %e, %Y" },
                },
                YAxis = new YAxis()
                {
                    Title = new Title()
                    {
                        Text = "no. of accesses"
                    }
                },
                Legend = new Legend() { Enabled = false }
            };
        }

        private IEnumerable<PayloadObject> GetOnlyDocuments()
        {


            return _accessDataCollection.Where(d => d.Type.ToLower() == "document" && d.ResourceProperties.Type.ToLower().Equals("document"));

        }

        /// <summary>
        /// Gets the top 10 documents.
        /// </summary>
        /// <returns></returns>
        public ChartConfig GetTop10Documents_Data()
        {
            // get top 10 documents (yAxis)
            var listData = GetOnlyDocuments().GroupBy(i => i.DocId).Select(q => new Data
            {
                X = q.Key,
                Y = q.Count()
            }).OrderByDescending(x => x.Y).Take(10).ToList();
            var yAxis = new YAxis()
            {
                Title = new Title() { Text = "no. of accesses" }
            };

            // get title for the documents
            var categories = new List<string>();
            listData.ToList().ForEach(t =>
            {
                int id = (int)t.X;
                var firstOrDefault = _resourceDataCollection.FirstOrDefault(e => e.Id == id);
                if (firstOrDefault != null)
                {
                    var name = firstOrDefault.Name ?? $"Cant find name for {id}";
                    categories.Add(name);
                }
                else
                {
                    categories.Add($"Cant find name for {id}");
                }
                t.X = null;
            });

            var xAxis = new XAxis()
            {
                Title = new Title()
                {
                    Text = "Name of Documents"
                },
                Type = "category",
                Categories = categories
            };

            return new ChartConfig
            {
                Title = new Title
                {
                    Text = "Top 10 Documents"
                },
                Series = new List<Series>(new Series[]
                {
                    new Series()
                    {
                        Name = "Accesses", Data = listData, Color="#7cb5ec"
                    }
                }),

                XAxis = xAxis,
                YAxis = yAxis,
                Options = new Options
                {
                    Chart = new Chart
                    {
                        Type = "column"
                    }
                }
            };
        }

        public IEnumerable<Data> GetLineChart()
        {

            var documents = _accessDataCollection.Where(o => o.Type == "document").GroupBy(i => i.AccessDate).Select(t => new Data
            {
                X = t.Key.ToJavaScriptMilliseconds(),
                Y = t.Count()
            }).OrderBy(o => o.X);


            return documents;
        }
        public IEnumerable<Data> GetLineChartFolders()
        {

            var folders = _accessDataCollection.Where(o => o.Referer != null && o.Referer.Type == "folder").GroupBy(i => i.AccessDate).Select(t => new Data
            {
                X = t.Key.ToJavaScriptMilliseconds(),
                Y = t.Count()
            }).OrderBy(o => o.X);


            return folders;
        }

        public IEnumerable<Data> GetLineChartFilteredDocuments(string docSubType, string user)
        {

            var documents = _accessDataCollection.Where(o => o.Type.ToLower().Equals("document") && (string.IsNullOrWhiteSpace(user) || o.Username.ToLower().Equals(user.ToLower()))
                && (string.IsNullOrWhiteSpace(docSubType) || o.ResourceProperties.SubType == docSubType))
                .GroupBy(i => i.AccessDate).Select(t => new Data
                {
                    X = t.Key.ToJavaScriptMilliseconds(),
                    Y = t.Count()
                }).OrderBy(o => o.X);



            return documents;
        }
        public IEnumerable<Data> GetLineChartFilteredFolders(string docSubType, string user)
        {

            var folders = _accessDataCollection.Where(o => o.Referer != null && o.Referer.Type == "folder"
                && (string.IsNullOrWhiteSpace(user) || o.Username.ToLower().Equals(user.ToLower())))
                .GroupBy(i => i.AccessDate).Select(t => new Data
                {
                    X = t.Key.ToJavaScriptMilliseconds(),
                    Y = t.Count()
                }).OrderBy(o => o.X);


            return folders;
        }


        public ChartConfig GetTop10Documents_FilteredData(string docType, string user)
        {

            //var ld = GetOnlyDocuments()
            //    .Where(t => t.ResourceProperties.SubType.Equals(docType, StringComparison.InvariantCultureIgnoreCase) &&
            //    (t.Username.Equals(user, StringComparison.InvariantCultureIgnoreCase) || string.IsNullOrWhiteSpace(user)));

            var ld = GetOnlyDocuments().Where(u => (string.IsNullOrWhiteSpace(docType) || u.ResourceProperties.SubType == docType) && (string.IsNullOrWhiteSpace(user) || u.Username == user));
            // get top 10 documents (yAxis)

            var listData = ld.GroupBy(i => i.DocId).Select(q => new Data
            {
                X = q.Key,
                Y = q.Count()
            }).OrderByDescending(x => x.Y).Take(10).ToList();




            var yAxis = new YAxis()
            {
                Title = new Title() { Text = "no. of accesses" }
            };

            // get title for the documents
            var categories = new List<string>();
            listData.ToList().ForEach(t =>
            {
                // int id = (int)t.X;                
                var firstOrDefault = _resourceDataCollection.FirstOrDefault(e => e.Id == t.X);
                if (firstOrDefault != null)
                {
                    // var name = firstOrDefault.Name ?? $"Cant find name for {id}";
                    categories.Add(firstOrDefault.Name);
                }
                else
                {
                    categories.Add($"Doc Id ({t.X})");
                }
                t.X = null;
            });

            var xAxis = new XAxis()
            {
                Title = new Title()
                {
                    Text = "Name of Document"
                },
                Type = "category",
                Categories = categories
            };

            return new ChartConfig
            {
                Title = new Title
                {
                    Text = "Top 10 Document"
                },
                Series = new List<Series>(new Series[]
                {
                    new Series()
                    {
                        Name = "Accesses", Data = listData, Color="#7cb5ec"
                    }
                }),
                XAxis = xAxis,
                YAxis = yAxis,
                Options = new Options
                {
                    Chart = new Chart
                    {
                        Type = "column"
                    }
                }
            };
        }


        public IEnumerable<UserData> GetUserList()
        {
            return _userDataCollection;
        }

        public IEnumerable<string> GetDocTypes()
        {
            List<string> types = new List<string>();

            foreach (var x in _resourceDataCollection)
            {
                if (!types.Contains(x.SubType))
                {
                    types.Add(x.SubType);
                }
            }
            return types;
        }

        public IEnumerable<ResourceData> GetResourceData()
        {
            return GetOnlyDocuments().OrderBy(d => d.DocId).Select(c => c.ResourceProperties).Distinct();
        }
    }


    public interface IReportsService
    {
        DouchNutData GetPreviousReferences(int documentId);
        DouchNutData GetNextReferences(int documentId);

        IEnumerable<string> GetDocTypes();
        IEnumerable<UserData> GetUserList();
        IEnumerable<ResourceData> GetResourceData();

        IEnumerable<Data> GetLineChartFolders();

        IEnumerable<Data> GetLineChartFilteredDocuments(string document, string user);
        IEnumerable<Data> GetLineChartFilteredFolders(string document, string user);

        ChartConfig GetDocsVFolders_Template();

        // Top 10 documents
        ChartConfig GetTop10Documents_Template();

        ChartConfig GetTop10Documents_Data();

        ChartConfig GetTop10Documents_FilteredData(string docType, string user);

        IEnumerable<Data> GetLineChart();
        // Top 10 search terms

        // 
    }

    public static class DateTimeJavaScript
    {
        private static readonly long DatetimeMinTimeTicks =
           (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Ticks;

        public static long ToJavaScriptMilliseconds(this DateTime dt)
        {
            return (long)((dt.ToUniversalTime().Ticks - DatetimeMinTimeTicks) / 10000);
        }
    }

    public abstract class ConcreteConfig : ChartConfig
    {
        
    }

    public abstract class Creator
    {
        public abstract ChartConfig FactoryMethod();
    }
    public class ConcreteCreator : Creator
    {
        public override ChartConfig FactoryMethod()
        {
            return new ChartConfig
            {
                Title = new Title
                {
                    Text = "Top 10 Documents"
                },
                Series = new List<Series>(new Series[]
                {
                    new Series()
                    {
                        Name = "Accesses",
                        Data = new List<Data>()
                    }
                }),
                Options = new Options
                {
                    Chart = new Chart
                    {
                        Type = "column"
                    }
                },
                XAxis = new XAxis()
                {
                    Title = new Title()
                    {
                        Text = "Name of Documents"
                    },
                    Type = "category",
                    Categories = new List<string>()
                },
                YAxis = new YAxis()
                {
                    Title = new Title()
                    {
                        Text = "no. of accesses"
                    }
                }

            };
        }
    }

}
