﻿'use strict';

(function () {
    var reportsApp = angular.module('reportsApp');

    reportsApp.controller('report1Ctrl', ['$scope', 'reportsSvc', '$http', '$state', function ($scope, reportsService, $http, $state) {
        var vm = this;
        vm.user;
        vm.documentType;
        vm.chartConfig = {};
        vm.userDropdown = [{}];
        vm.docTypeDropdown = [];

        // Init dropdowns
        $http.get('/reports/api/users').then(function (response) {
            vm.userDropdown = response.data;
        }, function (error) {
            //error
        });

            $http.get("/reports/api/docTypes").then(function (response) {
                        vm.docTypeDropdown = response.data;
                    }, function (error) {
                        //error
                    });

        $http.get('/reports/api/docstempl').then(function (response) {
            vm.chartConfig = response.data;
        }, function (error) { });

        vm.filter = function () {
            //$http.get('/reports/api/docs').then(function (response) {
            //    vm.chartConfig.xAxis.categories = response.data.xAxis.categories;
            //    vm.chartConfig.series = response.data.series;
            //    vm.chartConfig.series[0].point = {
            //        events: {
            //            click: function () { $state.go('report1params', { id: response.data.xAxis.categories[0] }); }
            //        }
            //    }
            //}, function (error) { });
                $http({
                    method: 'GET',
                    url: '/reports/api/documents',
                    params: {
                        documentType: vm.documentType || " ",
                        user: vm.user || " "
                    }
                }).then(function successCallback(response) {
                    vm.chartConfig.xAxis.categories = response.data.xAxis.categories;
                    vm.chartConfig.series = response.data.series;
                    vm.chartConfig.series[0].color = response.data.series[0].color;
                    vm.chartConfig.series[0].point = {
                        events: {
                            click: function (a, b) {
                                debugger;
                                $state.go('report1params', { id: response.data.xAxis.categories[a.point.index] });
                            }
                        }
                    }
                }, function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
        };
    }]);

})();