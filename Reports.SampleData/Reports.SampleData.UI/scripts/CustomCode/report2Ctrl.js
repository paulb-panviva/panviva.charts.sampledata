﻿'use strict';

(function () {
    var reportsApp = angular.module('reportsApp');

    reportsApp.controller('report2Ctrl', ['$scope', '$http','reportsSvc', function ($scope,$http, reportsService) {
        var vm = this;
        vm.user;
        vm.documentType;
        vm.chartConfig = {};
        vm.userDropdown = [{}];
        vm.docTypeDropdown = [];



        // Init dropdowns
        $http.get('/reports/api/users').then(function (response) {
            vm.userDropdown = response.data;
        }, function (error) {
            //error
        });

        $http.get("/reports/api/docTypes").then(function (response) {
            vm.docTypeDropdown = response.data;
        }, function (error) {
            //error
        });

        $http.get("/reports/api/docsVfoldersTemplate").then(function (response) {
            //success
            vm.chartConfig = response.data;
        }, function () {
            //failure

        });

        vm.filter = function () {
            //Documents

            $http({
                method: 'GET',
                url: '/reports/api/filteredDocuments',
                params: {
                    docSubType: vm.documentType || " ",
                    user: vm.user || " "
                }
            }).then(function (response) {
                var finalDataSet = [];
                for (var i = 0; i < response.data.length; i++) {
                    finalDataSet.push([response.data[i].x, response.data[i].y]);
                }
                vm.chartConfig.series[0].data = finalDataSet;
            });


            $http({
                method: 'GET',
                url: '/reports/api/filteredDocuments',
                params: {
                    docSubType: vm.documentType || " ",
                    user: vm.user || " "
                }
            }).then(function (response) {
                var finalDataSet = [];
                for (var i = 0; i < response.data.length; i++) {
                    finalDataSet.push([response.data[i].x, response.data[i].y]);
                }
                vm.chartConfig.series[0].data = finalDataSet;
            });



            //Folders

            $http({
                method: 'GET',
                url: '/reports/api/filteredFolders',
                params: {
                    docSubType: vm.documentType || " ",
                    user: vm.user || " "
                }
            }).then(function (response) {
                var finalDataSet = [];
                for (var i = 0; i < response.data.length; i++) {
                    finalDataSet.push([response.data[i].x, response.data[i].y]);
                }
                vm.chartConfig.series[1].data = finalDataSet;
            });
        }

        
            


    }]);

})();