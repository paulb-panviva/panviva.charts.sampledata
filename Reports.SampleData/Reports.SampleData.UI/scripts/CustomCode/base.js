﻿'use strict';

(function () {
    var reportsApp = angular.module('reportsApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'ui.bootstrap.tpls', 'ui.router', 'ui.utils', 'highcharts-ng']);

    reportsApp.config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/report1');
        $stateProvider
        // HOME STATES AND NESTED VIEWS ========================================
        .state('report1', {
                url: '/report1',
                templateUrl: '/Default/Report1'
        })
        .state('report1params', {
            url: '/report3/:id',
            templateUrl: '/Default/Report3'
        })
        .state('report2', {
            url: '/report2',
            templateUrl: '/Default/Report2'

        })
        .state('report3', {
            url: '/report3',
            templateUrl: '/Default/Report3'
        });
    });

    //reportsApp.factory('reportsSvc', function() {
    //    return {
    //        callReport: function() {
    //            return {};
    //        }
    //    }
    //});

    //reportsApp.controller('report1Ctrl', ['$scope', 'reportsSvc', function ($scope, reportsService) {

    //}]);

    //reportsApp.controller('report2Ctrl', ['$scope', 'reportsSvc', function ($scope, reportsService) {

    //}]);

    //reportsApp.controller('report3Ctrl', ['$scope', 'reportsSvc', function ($scope, reportsService) {

    //}]);

})();