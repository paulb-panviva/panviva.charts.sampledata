﻿'use strict';

(function () {
    var reportsApp = angular.module('reportsApp');

    reportsApp.controller('report3Ctrl', ['$scope', '$http', 'reportsSvc', '$stateParams', function ($scope, $http, reportsService, $stateParams) {
        debugger;
        var vm = this;
        //vm.idList = [{}];
        vm.user = "";
        vm.direction = "Previous";
        vm.isNext = false;

        var docName = $stateParams.id;

        vm.docCategories = [];
        vm.docData = [];

        vm.folCategories = [];
        vm.folData = [];

        vm.searCategories = [];
        vm.searData = [];

        vm.docCounter = 0;
        vm.folCounter = 0;
        vm.searCounter = 0;
        vm.docSelected = "";


        vm.callUrl = '/reports/api/from';
        

        var getNameForDocId = function (passedDocId) {
            for (var i = 0; i < vm.idList.length; i++) {
                if (vm.idList[i].id == passedDocId) {
                    return vm.idList[i].name;                    
                }
            }
        };

        $http.get('/reports/api/GetResourceData').then(function (response) {
            //success
            vm.idList = response.data;

            if (name != null) {
                for (var i = 0; i < vm.idList.length; i++) {
                    if (vm.idList[i].name == unescape(docName)) {
                        vm.user = vm.idList[i].id;
                        //vm.direction = "Next";
                        break;
                    }
                }
                vm.runReport();
            }
        });

        vm.reset = function () {
            vm.docCategories = [];
            vm.docData = [];

            vm.folCategories = [];
            vm.folData = [];

            vm.searCategories = [];
            vm.searData = [];

            vm.docCounter = 0;
            vm.folCounter = 0;
            vm.searCounter = 0;            
        };
        vm.reset();
        vm.runReport = function () {
            vm.reset();
            $http({
                method: 'GET',
                url: vm.callUrl,
                params: {
                    documentId: vm.user
                }
            }).then(function (response) {
                vm.previousData = response.data;                
                vm.docSelected = getNameForDocId(vm.previousData.documentId);

                for (var i = 0; i < vm.previousData.previous.length; i++) {
                    if (vm.previousData.previous[i].type == "Document") {
                        vm.docCategories.push(vm.previousData.previous[i].nameOrSearchTerm);
                        vm.docData.push(vm.previousData.previous[i].count);
                        vm.docCounter += vm.previousData.previous[i].count;
                    }

                    if (vm.previousData.previous[i].type == "Folder") {
                        vm.folCategories.push(vm.previousData.previous[i].nameOrSearchTerm);
                        vm.folData.push(vm.previousData.previous[i].count);
                        vm.folCounter += vm.previousData.previous[i].count;
                    }

                    if (vm.previousData.previous[i].type == "Search") {
                        vm.searCategories.push(vm.previousData.previous[i].nameOrSearchTerm);
                        vm.searData.push(vm.previousData.previous[i].count);
                        vm.searCounter += vm.previousData.previous[i].count;

                    }
                }
                buildData();
            });
        }

        var buildData = function () {
            var colors = Highcharts.getOptions().colors,
            categories = ['Documents', 'Search', 'Folder'],
            data = [{
                y: vm.docCounter,
                color: colors[0],
                drilldown: {
                    name: 'Documents',
                    categories: vm.docCategories,
                    data: vm.docData,
                    color: colors[0],
                }
            }, {
                y: vm.searCounter,
                color: colors[1],
                drilldown: {
                    name: 'Search',
                    categories: vm.searCategories,
                    data: vm.searData,
                    color: colors[1]
                }
            }, {
                y: vm.folCounter,
                color: colors[2],
                drilldown: {
                    name: 'Folder',
                    categories: vm.folCategories,
                    data: vm.folData,
                    color: colors[2]
                }
            }],
            browserData = [],
            versionsData = [],
            i,
            j,
            dataLen = data.length,
            drillDataLen,
            brightness;

            // Build the data arrays
            for (i = 0; i < dataLen; i += 1) {
                // add browser data
                browserData.push({
                    name: categories[i],
                    y: data[i].y,
                    color: data[i].color
                });

                // add version data
                drillDataLen = data[i].drilldown.data.length;
                for (j = 0; j < drillDataLen; j += 1) {
                    brightness = 0.2 - (j / drillDataLen) / 5;
                    versionsData.push({
                        name: data[i].drilldown.categories[j],
                        y: data[i].drilldown.data[j],
                        color: Highcharts.Color(data[i].color).brighten(brightness).get()
                    });
                }
            }

            // We have browser & version data!!
            vm.chartConfig.series[0].data = browserData;
            vm.chartConfig.series[1].data = versionsData;            
        }

        vm.onDirChange = function () {
            vm.chartConfig.title.text = vm.direction + ' documents';
            vm.chartConfig.series[0].data = vm.chartConfig.series[1].data = [];
            if (vm.direction == "Previous") {
                vm.callUrl = '/reports/api/from';
                vm.isNext = false;
            } else {
                vm.callUrl = '/reports/api/to';
                vm.isNext = true;
            }
        };

        
        vm.chartConfig = {
            options: {
                chart: {
                    type: 'pie'
                }
            },
            title: {
                text: vm.direction + ' documents'
            },
            plotOptions: {
                pie: {
                    shadow: false,
                    center: ['50%', '50%']
                }
            },
            tooltip: {
                valueSuffix: 'hits'
            },
            series: [{

                name: 'Hits',
                data: [],
                size: '60%',
                dataLabels: {
                    formatter: function () {
                        return this.y > 5 ? this.point.name : null;
                    },
                    color: '#ffffff',
                    distance: -30
                }
            }, {

                name: 'Hits',
                data: [],
                size: '80%',
                innerSize: '60%',
                dataLabels: {
                    formatter: function () {
                        // display only if larger than 1
                        return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + ' hits' : null;
                    }
                }
            }]
        };

    }]);

})();