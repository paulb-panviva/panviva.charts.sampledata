﻿using Reports.SampleData.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reports.SampleData.UI.ReportsClasses
{
    public class ChartConfig
    {
        public Title Title { get; set; }
        public Subtitle Subtitle { get; set; }
        public Options Options { get; set; }
        public Drilldown Drilldown { get; set; }
        public XAxis XAxis { get; set; }
        public YAxis YAxis { get; set; }
        public Legend Legend { get; set; }
        public IEnumerable<Series> Series { get; set; }

    }
}
