using Microsoft.Practices.Unity;
using Reports.SampleData.UI.Services;
using System.Web.Http;
using Unity.WebApi;

namespace Reports.SampleData.UI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IReportsService, ReportsService>(new ContainerControlledLifetimeManager());
            


            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }
    }
}