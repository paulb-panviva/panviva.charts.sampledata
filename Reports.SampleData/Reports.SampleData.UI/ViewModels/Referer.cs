﻿namespace TestingPayloadForCharts.Models
{
    public class Referer
    { 

        public string Type { get; set; }

        public string EquatedType { get {
                if (string.IsNullOrWhiteSpace(Type)) {
                    return "Unknown Origin";
                }
                if(Type.Equals("document", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    return DocId;
                }
                else if (Type.Equals("search", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    return $"ST~{Term}";
                }
                else if (Type.Equals("folder", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    return $"Folder";
                }

                return "Home Page";
            }
        }

        public string Term { get; set; }
        public string DocId { get; set; }
    }
}