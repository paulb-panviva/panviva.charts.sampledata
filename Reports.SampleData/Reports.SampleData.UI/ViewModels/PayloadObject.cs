﻿using System;
using TestingPayloadForCharts.Models;

namespace Reports.SampleData.UI.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class PayloadObject
    {
        public PayloadObject()
        {
            Referer = new Referer();
        }

        /// <summary>
        /// Gets or sets the referer.
        /// </summary>
        /// <value>
        /// The referer.
        /// </value>
        public Referer Referer;
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username { get; set; }
        /// <summary>
        /// Gets or sets the hostname.
        /// </summary>
        /// <value>
        /// The hostname.
        /// </value>
        public string Hostname { get; set; }
        /// <summary>
        /// Gets or sets the document identifier.
        /// </summary>
        /// <value>
        /// The document identifier.
        /// </value>
        public int DocId { get; set; }
        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        /// <value>
        /// The time.
        /// </value>
        public DateTime Time { get; set; }

        public DateTime AccessDate
        {
            get
            {
                return Time.Date;
            }
        }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type { get; set; }

        public ResourceData ResourceProperties { get; set; }
    }
}