﻿using System.Collections.Generic;

namespace Reports.SampleData.UI.ReportsClasses
{
    public class XAxis
    {
        public Title Title { get; set; }
        public string Type { get; set; }
        public List<string> Categories { get; set; }
        public DateTimeLabelFormats  DateTimeLabelFormats{ get; set; }
        public bool StartOnTick { get; set; }

    }
}