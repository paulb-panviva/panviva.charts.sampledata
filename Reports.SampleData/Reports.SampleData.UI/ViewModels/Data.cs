﻿using System;

namespace Reports.SampleData.UI.ReportsClasses
{
    /// <summary>
    /// 
    /// </summary>
    public class Data
    {
        /// <summary>
        /// </summary>
        private int? x;
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        public int Y { get; set; }
        /// <summary>
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        public long? X { get; set; }
        /// <summary>
        /// Gets or sets the drilldown.
        /// </summary>
        /// <value>
        /// The drilldown.
        /// </value>
        public string Drilldown { get; set; }
    }
}