﻿using System.Collections.Generic;

namespace Reports.SampleData.UI.ReportsClasses
{
    public class Series
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public List<Data> Data { get; set; }
    }
}