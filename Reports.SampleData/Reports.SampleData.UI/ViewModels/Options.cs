﻿namespace Reports.SampleData.UI.ReportsClasses
{
    public class Options
    {
        private int? turboThreshold;

        public Chart Chart { get; set; }
        public int? TurboThreshold { get { return turboThreshold; } set { turboThreshold = null; } }

    }
}