﻿namespace Reports.SampleData.UI.ReportsClasses
{
    public class Chart
    {
        public string Type { get; set; }
        public string ZoomType { get; set; }
    }
}