﻿using Reports.SampleData.UI.Services;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;

namespace Reports.SampleData.UI.Controllers.API
{
    [RoutePrefix("reports/api")]
    public class ReportsApiController : ApiController
    {
        private IReportsService _reportsService;
        public ReportsApiController(IReportsService repSer)
        {
            _reportsService = repSer;
        }

        // GET api/<controller>
        [Route("docstempl")]
        [HttpGet]
        public HttpResponseMessage GetTopDocumentAccess(HttpRequestMessage request)
        {

            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetTop10Documents_Template());
        }

        // GET api/<controller>
        [Route("from")]
        [HttpGet]
        public HttpResponseMessage GetReferencesFrom(HttpRequestMessage request, int documentId)
        {
            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetPreviousReferences(documentId));
        }

        // GET api/<controller>
        [Route("to")]
        [HttpGet]
        public HttpResponseMessage GetReferencesTo(HttpRequestMessage request, int documentId)
        {
            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetNextReferences(documentId));
        }

        [Route("docs")]
        [HttpGet]
        public HttpResponseMessage GetTopDocumentAccessData(HttpRequestMessage request)
        {

            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetTop10Documents_Data());
        }
        [Route("documents")]
        [HttpGet]
        public HttpResponseMessage GetTopDocumentAccessData(HttpRequestMessage request, string documentType, string user)
        {

            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetTop10Documents_FilteredData(documentType, user));
        }

        [Route("lineData")]
        [HttpGet]
        public HttpResponseMessage GetLineData(HttpRequestMessage request)
        {

            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetLineChart());
        }

        [Route("lineDataFolders")]
        [HttpGet]
        public HttpResponseMessage GetLineDataFolders(HttpRequestMessage request)
        {

            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetLineChartFolders());
        }



        [Route("docsVfoldersTemplate")]
        [HttpGet]
        public HttpResponseMessage GetDocumentVsFolderAccesses_Template(HttpRequestMessage request)
        {
            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetDocsVFolders_Template());
        }

        [Route("filteredDocuments")]
        [HttpGet]
        public HttpResponseMessage FilteredDocuments(HttpRequestMessage request, string user, string docSubType)
        {
            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetLineChartFilteredDocuments(docSubType,user));
        }
        [Route("filteredFolders")]
        [HttpGet]
        public HttpResponseMessage FilteredFolders(HttpRequestMessage request, string user, string docSubType)
        {
            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetLineChartFilteredFolders( docSubType, user));
        }



        [Route("users")]
        [HttpGet]
        public HttpResponseMessage GetUserList(HttpRequestMessage request)
        {
            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetUserList());
        }

        [Route("docTypes")]
        [HttpGet]
        public HttpResponseMessage GetDocTypeList(HttpRequestMessage request)
        {
            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetDocTypes());
        }

        [Route("GetResourceData")]
        [HttpGet]
        public HttpResponseMessage GetDocIDList(HttpRequestMessage request)
        {
            return request.CreateResponse(System.Net.HttpStatusCode.OK, _reportsService.GetResourceData());
        }
    }
}