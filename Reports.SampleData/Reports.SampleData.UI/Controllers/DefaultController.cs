﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reports.SampleData.UI.Controllers
{
    [Authorize]
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Report1()
        {
            return PartialView();
        }

        public ActionResult Report2()
        {
            return PartialView();
        }

        public ActionResult Report3()
        {
            return PartialView();
        }
    }
}