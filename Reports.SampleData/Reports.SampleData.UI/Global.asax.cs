﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Reports.SampleData.UI.App_Start;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Reports.SampleData.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            UnityConfig.RegisterComponents();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            UnityConfig.RegisterComponents(); 

            RouteConfig.RegisterRoutes(RouteTable.Routes);


            var formatters = GlobalConfiguration.Configuration.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

        }
    }
}
