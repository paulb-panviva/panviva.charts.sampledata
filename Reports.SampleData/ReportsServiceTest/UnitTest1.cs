﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Reports.SampleData.UI.Services;
using System.IO;
using System.Collections.Generic;

namespace ReportsServiceTest
{
    [TestClass]
    public class ReportsTest
    {
        [TestMethod]
        public void GetTop10DocumentsReturns10()
        {
            //arrange
            ReportsService reportsService = new ReportsService();

            //act
            var docs = reportsService.GetTop10Documents_Template();

            //assert
            Assert.AreEqual(10, docs.Series.FirstOrDefault().Data.Count);
        }

        [TestMethod]
        public void GetDocumentVsFolderAccesses()
        {
            //arrange
            ReportsService reportsService = new ReportsService();

            //act
            var result = reportsService.GetLineChartFolders();
     
            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void UniqueHostnames()
        {
            var accessJsonData = File.ReadAllText("C:\\Users\\PAULB\\Documents\\Visual Studio 2015\\Projects\\panviva.charts.sampledata\\Reports.SampleData\\Reports.SampleData.UI\\App_Data\\processed_filtered.json");
            var coll = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<Reports.SampleData.UI.ViewModels.PayloadObject>>(accessJsonData);
            
            var list = coll.GroupBy(g => g.Hostname).Select(s => s.Key);

            Assert.IsNotNull(list);
        }
    }
}
